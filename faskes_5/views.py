from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from datetime import datetime
from django.contrib import messages


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def ajaxLokasiFaskes(request):
    id_lokasi = request.GET.get('id_lokasi')
    print(id_lokasi)
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI FROM SIDIA.LOKASI WHERE ID = %s", [id_lokasi])
        data = cursor.fetchall()
    list_lokasi = []
    for i in data[0]:
        list_lokasi.append(i)

    return JsonResponse({'id': list_lokasi})

def ajaxTipeFaskes(request):
    kode_tipe_faskes = request.GET.get('kode_tipe_faskes')
    print(kode_tipe_faskes)
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT NAMA_TIPE FROM SIDIA.TIPE_FASKES WHERE KODE = %s", [kode_tipe_faskes])
        data = cursor.fetchall()
    list_tipe_faskes = []
    for i in data[0]:
        list_tipe_faskes.append(i)

    return JsonResponse({'id': list_tipe_faskes})



def create_faskes(request):
    form = CreateFaskesForm()
    if request.method == "POST":
        kode_faskes = request.POST['kode_faskes']
        id_lokasi = request.POST['id_lokasi']
        lokasi = request.POST['lokasi']
        petugas_faskes = request.POST['petugas_faskes']
        kode_tipe_faskes = request.POST['kode_tipe_faskes']
        tipe_faskes = request.POST['tipe_faskes']

        data_baru = [kode_faskes, id_lokasi, petugas_faskes, kode_tipe_faskes]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIDIA.FASKES VALUES (%s, %s, %s, %s)', data_baru)
        return redirect('faskes_5:readFaskes')
    return render(request, 'createFaskes.html', {'form': form})


def read_faskes(request):
    role = request.session['role']
    if role == "admin_satgas":
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO SIDIA;")
            sql = "SELECT DISTINCT kode_faskes_nasional,JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI, nama_tipe, username_petugas from faskes JOIN lokasi on id_lokasi=id JOIN tipe_faskes on kode_tipe_faskes=kode;"
            cursor.execute(sql)
            result = dictfetchall(cursor)
        return render(request, 'readFaskes.html', {'form': result})
    elif role == "supplier":
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO SIDIA;")
            sql = "SELECT DISTINCT kode_faskes_nasional,JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI, nama_tipe, username_petugas from faskes JOIN lokasi on id_lokasi=id JOIN tipe_faskes on kode_tipe_faskes=kode;"
            cursor.execute(sql)
            result = dictfetchall(cursor)
        return render(request, 'readFaskesS.html', {'form': result})
    else:
        pass


def delete_faskes(request):
    role = request.session['role']
    print(role)
    if role != 'admin_satgas':
        return redirect('home')
    if request.method == 'GET':
        if request.GET.get('kode_faskes_nasional') is not None:
            kode_faskes_nasional = request.GET.get('kode_faskes_nasional')
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIDIA.FASKES WHERE kode_faskes_nasional = %s", [kode_faskes_nasional])
                messages.success(request, ('Data berhasil dihapus'))

    return HttpResponseRedirect(reverse('faskes_5:readFaskes'))

def update_faskes(request):
    role = request.session['role']
    print(role)
    if role != 'admin_satgas':
        return redirect('home')
    if request.method == 'GET':
        print(request.GET.get('kode_faskes_nasional'))
        if request.GET.get('kode_faskes_nasional') is not None:
            kode_faskes_nasional = request.GET.get('kode_faskes_nasional')

            with connection.cursor() as cursor:
                cursor.execute("SELECT kode_faskes_nasional, id_lokasi,jalan_no,kelurahan,kecamatan,kabkot,provinsi,username_petugas,kode_tipe_faskes, nama_tipe FROM SIDIA.FASKES JOIN SIDIA.LOKASI ON ID = ID_LOKASI JOIN SIDIA.TIPE_FASKES ON KODE_TIPE_FASKES = KODE WHERE KODE_FASKES_NASIONAL = %s", [kode_faskes_nasional])
                result = dictfetchall(cursor)
            data_faskes = {}
            data_faskes['kode_faskes_nasional'] = result[0]['kode_faskes_nasional']
            data_faskes['id_lokasi'] = result[0]['id_lokasi']
            data_faskes['petugas_faskes'] = result[0]['username_petugas']
            data_faskes['lokasi'] = result[0]['jalan_no'],result[0]['kecamatan'], result[0]['kelurahan'],result[0]['kabkot']
            data_faskes['kode_tipe_faskes'] = result[0]['kode_tipe_faskes']
            data_faskes['tipe_faskes'] = result[0]['nama_tipe']
            form = UpdateFaskesForm(initial=data_faskes)
    else:
        kode_faskes_nasional = request.POST['kode_faskes_nasional']
        id_lokasi = request.POST['id_lokasi']
        petugas_faskes = request.POST['petugas_faskes']
        lokasi = request.POST['lokasi']
        kode_tipe_faskes = request.POST['kode_tipe_faskes'] 
        tipe_faskes = request.POST['tipe_faskes'] 

        data_update = [id_lokasi,petugas_faskes,kode_tipe_faskes,kode_faskes_nasional]
        with connection.cursor() as cursor:
            cursor.execute("UPDATE SIDIA.FASKES SET ID_LOKASI =%s, USERNAME_PETUGAS = %s,KODE_TIPE_FASKES = %s WHERE KODE_FASKES_NASIONAL = %s", data_update)
        return redirect('faskes_5:readFaskes')
    return render(request, 'updateFaskes.html', {'form': form})