from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from datetime import datetime
from django.contrib import messages

def create_batchpengiriman(request):
    form = CreateBatchPengirimanForm()

    if request.method == "POST":
        no_transaksi = request.POST['no_transaksi']
        petugas = request.POST['petugas']
        faskes = request.POST['faskes']
        berat = request.POST['berat']
        status_batch = request.POST['status_batch']

        data_baru = [no_transaksi, petugas, faskes, berat,status_batch ]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIDIA.BATCH_PENGIRIMAN VALUES (%s, %s, %s, %s,%s)', data_baru)
        return redirect('batchPengiirman_5:createBatchPengiriman')
    return render(request, 'createBatchPengiriman.html', {'form': form})

