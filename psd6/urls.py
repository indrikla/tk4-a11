from django.urls import path, include
from . import views

app_name = "psd6"
urlpatterns = [
    path('create', views.createPSD, name="createPSD"),
    path('getData', views.getData, name="getData"),
    path('list', views.listPSD, name="listPSD"),
    path('deleteDaftarItem/<nomorPesanan>/<noUrut>/', views.deleteDaftarItem, name="deleteDaftarItem"),
    path('delete/<nomorPesanan>/', views.deletePSD, name="deletePSD"),
    path('update/<nomorPesanan>/', views.updatePSD, name="updatePSD"),
    path('detail/<nomorPesanan>/', views.detailPSD, name="detailPSD"),
    path('tambahDaftarItem', views.tambahDaftarItem, name="tambahDaftarItem")
]