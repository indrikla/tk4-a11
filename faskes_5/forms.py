from django import forms
import datetime
from collections import namedtuple
from django.db import connection


class CreateFaskesForm(forms.Form):
    kode_faskes = forms.CharField(label='Kode Faskes', max_length=50)
    
    id_Lokasi_list = []
    id_lokasi = forms.ChoiceField(choices=id_Lokasi_list, required=True)
    
    lokasi = forms.CharField(label=("Lokasi"), required=True, max_length=50)

    petugas_faskes_list = []
    petugas_faskes = forms.ChoiceField(choices=petugas_faskes_list, required=True)
    
    
    kode_tipe_faskes_list = []
    kode_tipe_faskes = forms.ChoiceField(choices=kode_tipe_faskes_list, required=True)
    
    tipe_faskes = forms.CharField(label=("Tipe Faskes"), required=True, max_length=50)


    def __init__(self, *args, **kwargs):
        super(CreateFaskesForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT ID, ID FROM SIDIA.LOKASI")
            self.fields['id_lokasi'].choices = cursor.fetchall()
            cursor.execute("SELECT USERNAME, USERNAME FROM SIDIA.PETUGAS_FASKES")
            self.fields['petugas_faskes'].choices = cursor.fetchall()
            cursor.execute("SELECT KODE, KODE FROM SIDIA.TIPE_FASKES")
            self.fields['kode_tipe_faskes'].choices = cursor.fetchall()
    
            self.fields['id_lokasi'].widget.attrs = {'id': 'id_lokasi'}
            self.fields['lokasi'].widget.attrs = {'id': 'lokasi'}
            self.fields['kode_tipe_faskes'].widget.attrs = {'id': 'kode_tipe_faskes'}
            self.fields['tipe_faskes'].widget.attrs = {'id': 'tipe_faskes'}


class UpdateFaskesForm(forms.Form):
    kode_faskes_nasional = forms.CharField(label='Kode Faskes', max_length=50)
    
    id_lokasi_list = []
    id_lokasi = forms.ChoiceField(choices=id_lokasi_list, required=True)
    
    lokasi = forms.CharField(label=("Lokasi"), required=True, max_length=50)

    petugas_faskes_list = []
    petugas_faskes = forms.ChoiceField(choices=petugas_faskes_list, required=True)
    
    kode_tipe_faskes_list = []
    kode_tipe_faskes = forms.ChoiceField(choices=kode_tipe_faskes_list, required=True)
    
    tipe_faskes = forms.CharField(label=("Tipe Faskes"), required=True, max_length=50)


    def __init__(self, *args, **kwargs):
        super(UpdateFaskesForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            self.fields['kode_faskes_nasional'].widget.attrs['readonly'] = True
            self.fields['kode_faskes_nasional'].widget.attrs['class'] = 'disabled'
            cursor.execute("SELECT ID, ID FROM SIDIA.LOKASI")
            self.fields['id_lokasi'].choices = cursor.fetchall()
            self.fields['id_lokasi'].widget.attrs = {'id': 'id_lokasi'}
            cursor.execute("SELECT USERNAME, USERNAME FROM SIDIA.PETUGAS_FASKES")
            self.fields['petugas_faskes'].choices = cursor.fetchall()
            cursor.execute("SELECT KODE, KODE FROM SIDIA.TIPE_FASKES")
            self.fields['kode_tipe_faskes'].choices = cursor.fetchall()
            self.fields['lokasi'].widget.attrs['readonly'] = True
            self.fields['lokasi'].widget.attrs = {'id': 'lokasi'}
            self.fields['tipe_faskes'].widget.attrs['readonly'] = True
            self.fields['tipe_faskes'].widget.attrs = {'id': 'tipe_faskes'}
            self.fields['kode_tipe_faskes'].widget.attrs = {'id': 'kode_tipe_faskes'}

