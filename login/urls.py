from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('login', views.login, name="login"),
    path('logout', views.logout, name="logout"),
    path('registerPilihan', views.registerPilihan, name="registerPilihan"),
    path('regadminsatgas', views.register_adminsatgas, name="regadminsatgas"),
    path('regsupplier', views.register_supplier, name="regsupplier"),
    path('regpetugasdistribusi', views.register_petugasdistribusi, name="regpetugasdistribusi"),
    path('regpetugasfaskes', views.register_petugasfaskes, name="regpetugasfaskes"),
    path('dashboard', views.dashboard, name="dashboard"),
    path('profile', views.profile, name='profile'),
]