from django import forms
from django.db import connection
from django.forms import widgets


class CreateBatchPengirimanForm(forms.Form):
    petugas_satgas = forms.CharField(label='Petugas Satgas', disabled=True)

    nomor_transaksi = forms.CharField(label='Nomor Transaksi Sumber Daya', disabled=True)

    kode_batch = forms.CharField(label='Kode Batch', disabled=True)

    list_kendaraan = {}
    kendaraan = forms.ChoiceField(choices=list_kendaraan, required=True)

    list_petugas = {}
    petugas = forms.ChoiceField(choices=list_petugas, required=True)

    def __init__(self, *args, **kwargs):
        super(CreateBatchPengirimanForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            # cursor.execute("SELECT * FROM sidia.BATCH_PENGIRIMAN b WHERE KODE NOT IN (SELECT kode_status_batch_pengiriman FROM sidia.RIWAYAT_STATUS_PENGIRIMAN r WHERE r.kode_batch = b.kode) LIKE 'PRO' OR KODE NOT IN (SELECT kode_status_batch_pengiriman FROM sidia.RIWAYAT_STATUS_PENGIRIMAN r WHERE r.kode_batch = b.kode) LIKE 'OTW'")
            # self.fields['kendaraan'].choices = cursor.fetchall()
            self.fields['nomor_transaksi'].widget.attrs = {'id': 'nomor_transaksi'}
            self.fields['kode_batch'].widget.attrs = {'id': 'kode_batch'}
            self.fields['petugas'].widget.attrs = {'id': 'petugas'}
            self.fields['petugas_satgas'].widget.attrs = {'id': 'petugas_satgas'}

