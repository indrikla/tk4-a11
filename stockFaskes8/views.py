from django.shortcuts import render
from django.shortcuts import render, reverse, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.db import connection
from collections import namedtuple
import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def getDataFaskes(request):
    kodeFaskes = request.GET.get('kodeFaskes')
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    sql = "SELECT DISTINCT kode_faskes_nasional, nama from faskes  JOIN pengguna p on username_petugas=username where kode_faskes_nasional=%s;"
    cursor.execute(sql, [kodeFaskes])
    result = namedtuplefetchall(cursor)
    args = {"faskes" : result}
    return JsonResponse(args)

def getDataItemSD(request):
    kodeItem = request.GET.get('kodeItem')
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    sql = ("select nama from stok_faskes JOIN item_sumber_daya ON kode_item_sumber_daya=kode JOIN faskes ON kode_faskes = kode_faskes_nasional where kode=%s;")
    cursor.execute(sql, [kodeItem])
    result = namedtuplefetchall(cursor)
    args = {"itemSumberDaya" : result}
    return JsonResponse(args)

def createStokFaskes(request):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    if request.method == "POST":
        kodeFaskesForm = request.POST.get("kodeFaskesForm")
        faskesForm = request.POST.get("faskesForm")
        kodeItemForm = request.POST.get("kodeItemForm")
        itemSDForm = request.POST.get("itemSDForm")
        jumlahForm = request.POST.get("jumlahForm")

        stokFaskesInsert = "INSERT INTO stok_faskes VALUES(%s, %s, %s)"
        cursor.execute(stokFaskesInsert, [kodeFaskesForm, kodeItemForm, jumlahForm])
        return redirect('listStokFaskes')

    sql = "select kode_faskes, username_petugas, kode, nama, jumlah from stok_faskes JOIN item_sumber_daya ON kode_item_sumber_daya=kode JOIN faskes ON kode_faskes = kode_faskes_nasional;"
    cursor.execute(sql)

    allFaskes = namedtuplefetchall(cursor)

    sqlKodeFaskes = "SELECT DISTINCT kode_faskes_nasional FROM faskes;"
    cursor.execute(sqlKodeFaskes)
    kodeFaskes = namedtuplefetchall(cursor)

    sqlKodeItem = "select kode from item_sumber_daya;"
    cursor.execute(sqlKodeItem)
    kodeItem = namedtuplefetchall(cursor)

    return render(request, "createStokFaskes.html", {"kodeFaskes" : kodeFaskes, "allFaskes" : allFaskes, "kodeItem" : kodeItem})

def deleteStokFaskes(request, kodeFaskes, kodeISD):
    cursor = connection.cursor()
    cursor.execute("SET search_path to sidia")
    cursor.execute("DELETE from stok_faskes where kode_faskes=%s AND kode_item_sumber_daya=%s", [kodeFaskes, kodeISD])
    return redirect('listStokFaskes')

def updateStokFaskes(request, kodeFaskes, kodeISD):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    if request.method == "POST":
        jumlahForm = request.POST.get("jumlahForm")

        print(request.POST)

        stokFaskesUpdate = "UPDATE stok_faskes SET jumlah =%s WHERE kode_faskes=%s AND kode_item_sumber_daya=%s"
        cursor.execute(stokFaskesUpdate, [jumlahForm, kodeFaskes, kodeISD])
        return redirect('listStokFaskes')

    sql = "select kode_faskes, p.nama as namaLengkap, kode, isd.nama as namaItem, jumlah from stok_faskes JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode JOIN faskes ON kode_faskes = kode_faskes_nasional JOIN pengguna p on username_petugas=username WHERE kode_faskes=%s AND kode=%s;"
    cursor.execute(sql, [kodeFaskes, kodeISD])
    updateFaskes = namedtuplefetchall(cursor)

    return render(request, "updateStokFaskes.html", {"updateFaskes" : updateFaskes})

def listStokFaskes(request):
    print(request.session.keys())
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    username = request.session['username']
    role = request.session['role']
    if role == "admin_satgas":
        sql = "select kode_faskes, p.nama as namaLengkap, kode, isd.nama as namaItem, jumlah from stok_faskes JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode JOIN faskes ON kode_faskes = kode_faskes_nasional JOIN pengguna p on username_petugas=username;"
        cursor.execute(sql)
        listFaskes = namedtuplefetchall(cursor)
        return render(request, "listStokFaskesAS.html", {"listFaskes" : listFaskes})
    elif role == "petugas_faskes":
        sql = "select kode_faskes, p.nama as namaLengkap, kode, isd.nama as namaItem, jumlah from stok_faskes JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode JOIN faskes ON kode_faskes = kode_faskes_nasional JOIN pengguna p on username_petugas=username WHERE username_petugas=%s;"
        cursor.execute(sql, [username])
        listFaskes = namedtuplefetchall(cursor)
        return render(request, "listStokFaskes.html", {"listFaskes" : listFaskes})
    else:
        pass