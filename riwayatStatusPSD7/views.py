from django.shortcuts import render
from django.shortcuts import render, reverse, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.db import connection
from collections import namedtuple
import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def listRiwayatPSD(request, nomorPesanan):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    sql = "SELECT no_pesanan, kode_status_pesanan, nama, username_supplier, tanggal FROM sidia.riwayat_status_pesanan join sidia.status_pesanan ON kode_status_pesanan=kode WHERE no_pesanan=%s ORDER BY tanggal DESC"
    cursor.execute(sql, [nomorPesanan])
    listRiwayatPSD = namedtuplefetchall(cursor)
    print(listRiwayatPSD)
    return render(request, "listRiwayatPSD.html", {"listRiwayatPSD" : listRiwayatPSD})

def createRiwayatPSD(request, nomorPesanan, status):
    print("masuk")
    username = request.session['username']
    tanggal = datetime.datetime.now()
    cursor = connection.cursor()
    #ALTER TABLE riwayat_status_pesanan ALTER COLUMN tanggal TYPE TIMESTAMP;
    # py manage.py runserver
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    sql = "SELECT no_pesanan, kode_status_pesanan, nama, username_supplier, DATE(tanggal) FROM sidia.riwayat_status_pesanan join sidia.status_pesanan ON kode_status_pesanan=kode WHERE no_pesanan=%s AND kode_status_pesanan=%s ORDER BY tanggal DESC"
    cursor.execute(sql, [nomorPesanan, status])
    listRiwayatPSD = namedtuplefetchall(cursor)
    print(listRiwayatPSD)
    sqlRiwayatPSDCreate = "INSERT INTO RIWAYAT_STATUS_PESANAN VALUES (%s, %s, %s, %s)"
    cursor.execute(sqlRiwayatPSDCreate, [status, nomorPesanan, username, tanggal])
    print(tanggal)
    return redirect('/PSD/list')