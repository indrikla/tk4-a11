from django.shortcuts import render
from django.shortcuts import render, reverse, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection
from collections import namedtuple
from django.http import HttpResponse, JsonResponse
from .forms import LoginForm, RegAdminSatgas, RegSupplier, RegPetugasDistribusi, RegPetugasFaskes

    
def home(request):
    return render(request, "home.html")
    
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def login(request) :
    message = ''
    if request.method =='POST':
        username = request.POST.get("username")
        password = request.POST.get("password")

        cursor = connection.cursor()

        sql = "select username, password from sidia.pengguna where username = " + "'" + username + "'" 
        cursor.execute(sql)
        auth = cursor.fetchone()
        print(auth)
            
        message = 'Invalid username or password'
        if auth != None:
            if password == auth[1]:
                request.session['username'] = username
                if search_role(username, 'admin_satgas'):
                    request.session['role'] = 'admin_satgas'
                elif search_role(username, 'supplier'):
                    request.session['role'] = 'supplier'
                elif search_role(username, 'petugas_distribusi'):
                    request.session['role'] = 'petugas_distribusi'
                elif search_role(username, 'petugas_faskes'):
                    request.session['role'] = 'petugas_faskes'
                print(request.session['username'])
                print(request.session['role'])
                return redirect('/dashboard')
            
    form = LoginForm()
    args = {'form': form, 'message' : message }
    return render(request, 'login.html', args)

def dashboard(request):
    try:
        username = request.session['username']
        role = request.session['role']

        if role == "admin_satgas":
           return render(request, "dashboardAS.html")
        elif role == "petugas_distribusi":
           return render(request, "dashboardPD.html")
        elif role == "petugas_faskes":
           return render(request, "dashboardPF.html")
        elif role == "supplier":
           return render(request, "dashboardS.html")
        else:
            return redirect('/')
    except:
            return redirect('/')

def search_role(username, role):
    cursor = connection.cursor()
    sql = "select * from sidia."+ role + " where username ='" + username + "';"
    cursor.execute(sql)
    sql_role = namedtuplefetchall(cursor)
    if len(sql_role) == 0:
            return False
    else:
            return True

def logout(request):
     try:
          del request.session['username']
          del request.session['role']
          print("log out success")
          return redirect('/')
     except:
          pass
     return redirect('/')

def registerPilihan(request):
    return render(request, "registerPilihan.html")

def register_adminsatgas(request):
    message = ' '
    success = "False"
    role = "admin_satgas"
    if request.method =='POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        nama = request.POST.get("nama")
        kelurahan = request.POST.get("kelurahan")
        kecamatan = request.POST.get("kecamatan")
        kabupatankota = request.POST.get("kabupatankota")
        provinsi = request.POST.get("provinsi")
        no_telp = request.POST.get("no_telp")
        

        cursor = connection.cursor()

        sql_count_pengguna = "select count(*) from sidia.pengguna where username = " + "'" + username + "'" 
        cursor.execute(sql_count_pengguna)
        count_pengguna = cursor.fetchone()

        sql_count_admin = "select count(*) from sidia.admin_satgas where username = " + "'" + username + "'" 
        cursor.execute(sql_count_admin)
        count_admin = cursor.fetchone()
        
        if count_pengguna[0] == 0 and count_admin[0] == 0 :
                sql_pengguna = "insert into sidia.pengguna VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"
                cursor.execute(sql_pengguna,(username, password, nama, kelurahan, kecamatan, kabupatankota, provinsi , no_telp))

                sql_admin = "insert into sidia.admin_satgas VALUES ('" + username + "');"
                cursor.execute(sql_admin)
                message = 'Pendaftaran sebagai ' + role + ' berhasil dilakukan'
                success = "True"
                return redirect('login')

        else: 
            success = "False"
            message = 'Username tersebut sudah terdaftar dengan peran ' + role

    form = RegAdminSatgas()
    args = {'form': form, 'message' : message, 'success': success, 'role' : role}
    return render(request, 'register.html', args)


def register_supplier(request):
    message = ' '
    success = "False"
    role = "supplier"
    if request.method =='POST':
        organisasi = request.POST.get("organisasi")
        username = request.POST.get("username")
        password = request.POST.get("password")
        nama = request.POST.get("nama")
        kelurahan = request.POST.get("kelurahan")
        kecamatan = request.POST.get("kecamatan")
        kabupatankota = request.POST.get("kabupatankota")
        provinsi = request.POST.get("provinsi")
        no_telp = request.POST.get("no_telp")
        

        cursor = connection.cursor()

        sql_count_pengguna = "select count(*) from sidia.pengguna where username = " + "'" + username + "'" 
        cursor.execute(sql_count_pengguna)
        count_pengguna = cursor.fetchone()

        sql_count_supplier = "select count(*) from sidia.supplier where username = " + "'" + username + "'" 
        cursor.execute(sql_count_supplier)
        count_supplier = cursor.fetchone()
        

        if count_pengguna[0] == 0 and count_supplier[0] == 0 :
            sql_pengguna = "insert into sidia.pengguna VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"
            cursor.execute(sql_pengguna,(username, password, nama, kelurahan, kecamatan, kabupatankota, provinsi , no_telp))

            sql_supplier = "insert into sidia.supplier VALUES ('" + username + "', '" + organisasi + "');"
            cursor.execute(sql_supplier)
            return redirect('login')
        else: 
            success = "False"
            message = 'Username tersebut sudah terdaftar dengan peran ' + role

    form = RegSupplier()
    args = {'form': form, 'message' : message, 'success': success, 'role' : role}
    return render(request, 'register.html', args)

def register_petugasdistribusi(request):
    message = ' '
    success = "False"
    role = "petugas_distribusi"
    if request.method =='POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        nama = request.POST.get("nama")
        kelurahan = request.POST.get("kelurahan")
        kecamatan = request.POST.get("kecamatan")
        kabupatankota = request.POST.get("kabupatankota")
        provinsi = request.POST.get("provinsi")
        no_telp = request.POST.get("no_telp")
        no_sim = request.POST.get("no_sim")
        

        cursor = connection.cursor()

        sql_count_pengguna = "select count(*) from sidia.pengguna where username = " + "'" + username + "'" 
        cursor.execute(sql_count_pengguna)
        count_pengguna = cursor.fetchone()

        sql_count_dist = "select count(*) from sidia.petugas_distribusi where username = " + "'" + username + "'" 
        cursor.execute(sql_count_dist)
        count_dist = cursor.fetchone()
    

        if count_pengguna[0] == 0 and count_dist[0] == 0 :
            sql_pengguna = "insert into sidia.pengguna VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"
            cursor.execute(sql_pengguna,(username, password, nama, kelurahan, kecamatan, kabupatankota, provinsi , no_telp))

            sql_count_dist = "insert into sidia.petugas_distribusi VALUES ('" + username + "', '" + no_sim + "');"
            cursor.execute(sql_count_dist)
            return redirect('login')
        else: 
            success = "False"
            message = 'Username tersebut sudah terdaftar dengan peran ' + role

    form = RegPetugasDistribusi()
    args = {'form': form, 'message' : message, 'success': success, 'role' : role}
    return render(request, 'register.html', args)

def register_petugasfaskes(request):
    message = ' '
    role = "petugas_faskes"
    success = "False"
    if request.method =='POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        nama = request.POST.get("nama")
        kelurahan = request.POST.get("kelurahan")
        kecamatan = request.POST.get("kecamatan")
        kabupatankota = request.POST.get("kabupatankota")
        provinsi = request.POST.get("provinsi")
        no_telp = request.POST.get("no_telp")

        cursor = connection.cursor()

        sql_count_pengguna = "select count(*) from sidia.pengguna where username = " + "'" + username + "'" 
        cursor.execute(sql_count_pengguna)
        count_pengguna = cursor.fetchone()

        sql_count_petfaskes = "select count(*) from sidia.petugas_faskes where username = " + "'" + username + "'" 
        cursor.execute(sql_count_petfaskes)
        count_petfaskes = cursor.fetchone()
        

        if count_pengguna[0] == 0 and count_petfaskes[0] == 0 :
            sql_pengguna = "insert into sidia.pengguna VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"
            cursor.execute(sql_pengguna,(username, password, nama, kelurahan, kecamatan, kabupatankota, provinsi , no_telp))

            sql_count_petfaskes = "insert into sidia.petugas_faskes VALUES ('" + username + "');"
            cursor.execute(sql_count_petfaskes)
            return redirect('login')

        else: 
            success = "False"
            message = 'Username tersebut sudah terdaftar dengan peran ' + role

    form = RegPetugasFaskes()
    args = {'form': form, 'message' : message, 'success': success, 'role' : role}
    return render(request, 'register.html', args)

def profile(request):
    print(request.session.keys())
    cursor = connection.cursor()
    cursor.execute("set search_path to sidia")
    try:
        username = request.session['username']
        role = request.session['role']
        if request.method == "POST":
            namaNew = request.POST.get("namaForm")
            sqlGantiNama = "UPDATE PENGGUNA SET nama=%s WHERE username=%s"
            cursor.execute(sqlGantiNama, [namaNew, username])
            return redirect('profile')

        if role == "admin_satgas":
            sqlDataPengguna = "SELECT * FROM ADMIN_SATGAS NATURAL JOIN PENGGUNA WHERE username=%s"
            cursor.execute(sqlDataPengguna, [username])
            dataPengguna = namedtuplefetchall(cursor)
            args = {'args': dataPengguna[0]}
            print(dataPengguna)
            return render(request, "profileAS.html", args)

        elif role == "petugas_distribusi":
            sqlDataPengguna = "SELECT * FROM petugas_distribusi NATURAL JOIN PENGGUNA WHERE username=%s"
            cursor.execute(sqlDataPengguna, [username])
            dataPengguna = namedtuplefetchall(cursor)
            args = {'args': dataPengguna[0]}
            return render(request, "profilePD.html", args)

        elif role == "petugas_faskes":
            sqlDataPengguna = "SELECT * FROM petugas_faskes NATURAL JOIN PENGGUNA WHERE username=%s"
            cursor.execute(sqlDataPengguna, [username])
            dataPengguna = namedtuplefetchall(cursor)
            args = {'args': dataPengguna[0]}
            return render(request, "profilePF.html", args)

        elif role == "supplier":
            sqlDataPengguna = "SELECT * FROM supplier NATURAL JOIN PENGGUNA WHERE username=%s"
            cursor.execute(sqlDataPengguna, [username])
            dataPengguna = namedtuplefetchall(cursor)
            args = {'args': dataPengguna[0]}
            print(args)
            return render(request, "profileSup.html", args)
        else:
            return redirect('/')
    except:
            return redirect('/')

# def updateNama(request):
#     print(request.session.keys())
#     cursor = connection.cursor()
#     cursor.execute("set search_path to sidia")
#     try:
#         username = request.session['username']
        
#     except:
#         redirect('profile')

#     return redirect('profile')