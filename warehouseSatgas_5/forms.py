from django import forms
import datetime
from collections import namedtuple
from django.db import connection


class CreateWarehouseSatgasForm(forms.Form):
    provinsi_list = []
    provinsi = forms.ChoiceField(choices=provinsi_list, required=True)
    
    id_lokasi_list = []
    id_lokasi = forms.ChoiceField(choices=id_lokasi_list, required=True)
    
    lokasi = forms.CharField(label=("Lokasi"), required=True, max_length=50)


    def __init__(self, *args, **kwargs):
        super(CreateWarehouseSatgasForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT DISTINCT PROVINSI, PROVINSI FROM SIDIA.LOKASI,SIDIA.WAREHOUSE_PROVINSI WHERE LOKASI.ID NOT IN (SELECT ID_LOKASI FROM SIDIA.WAREHOUSE_PROVINSI)")
            self.fields['provinsi'].choices = cursor.fetchall()
            cursor.execute("SELECT ID, ID FROM SIDIA.LOKASI")
            
            self.fields['provinsi'].widget.attrs = {'id': 'provinsi'}
            self.fields['id_lokasi'].widget.attrs = {'id': 'id_lokasi'}
            self.fields['lokasi'].widget.attrs = {'id': 'lokasi'}       

class UpdateWarehouseSatgasForm(forms.Form):
    provinsi = forms.CharField(label=("Provinsi"), required=True, max_length=50)

    id_lokasi_list = []
    id_lokasi = forms.ChoiceField(choices=id_lokasi_list, required=True)
   
    lokasi = forms.CharField(label=("Lokasi"), required=True, max_length=50)


    def __init__(self, *args, **kwargs):
        super(UpdateWarehouseSatgasForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            
            self.fields['provinsi'].widget.attrs = {'id': 'provinsi'}
            self.fields['id_lokasi'].widget.attrs = {'id': 'id_lokasi'}
            self.fields['lokasi'].widget.attrs = {'id': 'lokasi'}