from django.shortcuts import render
from django.shortcuts import render, reverse, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.db import connection
from collections import namedtuple
import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def listPSD(request):
    print(request.session.keys())
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDIA;")
    username = request.session['username']
    role = request.session['role']

    if role == "admin_satgas":
        cursor.execute("CREATE VIEW sqlTemp AS SELECT DISTINCT ON (nomor_pesanan) nomor_pesanan, username_admin_satgas, rsp.tanggal, total_berat, total_harga, total_item, kode_status_pesanan from pesanan_sumber_daya JOIN transaksi_sumber_daya tsd on nomor_pesanan=nomor JOIN riwayat_status_pesanan rsp ON nomor_pesanan=no_pesanan ORDER BY nomor_pesanan, rsp.tanggal DESC;")
        sqlListPSDView = "SELECT * FROM sqlTemp WHERE username_admin_satgas=%s"
        cursor.execute(sqlListPSDView, [username])
        listPSD = namedtuplefetchall(cursor)
        print(listPSD)
        cursor.execute("DROP VIEW sqlTemp")
        args = {"listPSD": listPSD} 
        return render(request, "listPSDAS.html", args)

    elif role == "supplier":
        cursor.execute("CREATE VIEW sqlTemp AS SELECT DISTINCT ON (nomor_pesanan) nomor_pesanan, username_admin_satgas, username_supplier, rsp.tanggal, total_berat, total_harga, total_item, kode_status_pesanan from pesanan_sumber_daya JOIN transaksi_sumber_daya tsd on nomor_pesanan=nomor JOIN riwayat_status_pesanan rsp ON nomor_pesanan=no_pesanan ORDER BY nomor_pesanan, rsp.tanggal DESC;")
        sqlListPSDView = "SELECT * FROM sqlTemp WHERE username_supplier=%s"
        cursor.execute(sqlListPSDView, [username])
        listPSD = namedtuplefetchall(cursor)
        print(listPSD)
        cursor.execute("DROP VIEW sqlTemp")
        args = {"listPSD": listPSD}             
        return render(request, "listPSDSup.html", args)
        
    else:
        pass
    

def getData(request):
    cursor = connection.cursor()
    if request.method == 'GET':
        selectedSupplier = request.GET.get('supplier')
        print(selectedSupplier)
        sqlKode = "SELECT kode FROM sidia.item_sumber_daya WHERE username_supplier=" + "'" + selectedSupplier + "';"
   
    kodeItemListTemp = cursor.execute(sqlKode)
    kodeItemList = namedtuplefetchall(cursor)
    kodeItemListFinal = []
    for kode in kodeItemList:
        kodeItemListFinal.append(kode.kode)
    print(kodeItemListFinal)

    args = {"kodeItemList": kodeItemListFinal, "selectedSupplier" : selectedSupplier} 
    return JsonResponse(args)

def tambahDaftarItem(request):
    cursor = connection.cursor()
    if request.method == 'GET':
        kodeItem = request.GET.get('kodeItem')
        sql = "SELECT harga_satuan FROM sidia.item_sumber_daya WHERE kode=" + "'" + kodeItem + "';"
   
    cursor.execute(sql)
    hargaSatuan = namedtuplefetchall(cursor)

    cursor.execute("SELECT nama FROM sidia.item_sumber_daya WHERE kode=" + "'" + kodeItem + "';")
    namaItem = namedtuplefetchall(cursor)

    args = {"hargaSatuan": hargaSatuan, 'namaItem' : namaItem}
    return JsonResponse(args)

def createPSD(request):
    usernameAdmin = request.session['username']
    cursor = connection.cursor()
    if request.method =='POST':
        noTransaksiForm = request.POST.get("noTransaksiForm")
        petugasForm = request.POST.get("petugasForm")
        supplierForm = request.POST.get("supplierForm")
        daftarItem = request.POST.getlist("daftarItem[]")
        tanggal = datetime.datetime.now()

        # sqlTransaksiSumberDaya = "INSERT INTO sidia.transaksi_sumber_daya (tanggal) VALUES (%s);"
        sqlTransaksiSumberDaya = "INSERT INTO sidia.transaksi_sumber_daya VALUES (%s, %s);"
        cursor.execute(sqlTransaksiSumberDaya, [noTransaksiForm, str(tanggal)])
        # cursor.execute("SELECT LASTVAL() FROM sidia.transaksi_sumber_daya;")
        # noTransaksiForm = namedtuplefetchall(cursor)
        # print(noTransaksiForm)

        sqlDaftarItem = "INSERT INTO sidia.daftar_item VALUES (%s, %s, %s, %s)"
        for i in range(0, len(daftarItem), 3):
            cursor.execute(sqlDaftarItem, (noTransaksiForm, daftarItem[i], daftarItem[i+1], daftarItem[i+2]))
        
        # sqlHargaSatuan = "SELECT harga_kumulatif FROM sidia.daftar_item WHERE no_transaksi_sumber_daya=%s"
        # cursor.execute(sqlHargaSatuan, [noTransaksiForm])
        # hargaSatuanTemp = namedtuplefetchall(cursor)
        # hargaTotal = 0
        # for i in range(0, len(hargaSatuanTemp)):
        #     hargaTotal += hargaSatuanTemp[i].harga_kumulatif
        # print(hargaSatuanTemp)
        # print(hargaTotal)

        sqlPesananSumberDaya = "INSERT INTO sidia.pesanan_sumber_daya VALUES (%s, %s);"
        cursor.execute(sqlPesananSumberDaya, [noTransaksiForm, usernameAdmin])

        sqlRiwayatStatusPesanan = "INSERT INTO sidia.RIWAYAT_STATUS_PESANAN VALUES (%s, %s, %s, %s);"
        cursor.execute(sqlRiwayatStatusPesanan, ["REQ-SUP", noTransaksiForm, supplierForm, str(tanggal)])
        return redirect('psd6:listPSD')


    noTransaksi = cursor.execute("SELECT MAX(nomor) FROM sidia.transaksi_sumber_daya;")
    noTransaksiTemp = namedtuplefetchall(cursor)
    noTransaksiInt = noTransaksiTemp[0].max + 1

    supplierListTemp = cursor.execute("SELECT DISTINCT username FROM sidia.supplier;")
    supplierList = namedtuplefetchall(cursor)

    args = {"noTransaksi": noTransaksiInt, "petugasCurrent": usernameAdmin, "supplierList" : supplierList}
    return render(request, "createPSD.html", args)

def deletePSD(request, nomorPesanan):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidia")
    cursor.execute("delete from transaksi_sumber_daya where nomor = '" + nomorPesanan + "'")
    cursor.execute("delete from pesanan_sumber_daya where nomor_pesanan = '" + nomorPesanan + "'")
    cursor.execute("delete from daftar_item where no_transaksi_sumber_daya = '" + nomorPesanan + "'")
    cursor.execute("delete from riwayat_status_pesanan where no_pesanan = '" + nomorPesanan + "'")
    return redirect('psd6:listPSD')

def updatePSD(request, nomorPesanan):
    usernameAdmin = request.session['username']
    cursor = connection.cursor()
    cursor.execute("set search_path to sidia")
    if request.method =='POST':
        noTransaksiForm = request.POST.get("noTransaksiForm")
        petugasForm = request.POST.get("petugasForm")
        supplierForm = request.POST.get("supplierForm")
        daftarItem = request.POST.getlist("daftarItem[]")
        yangDiDelete = request.POST.getlist("yangDiDelete[]")
        tanggal = datetime.datetime.now()

        sqlDaftarItem = "INSERT INTO sidia.daftar_item VALUES (%s, %s, %s, %s)"
        sqlRetriveKode = "SELECT kode_item_sumber_daya, no_urut FROM daftar_item WHERE no_transaksi_sumber_daya =%s"
        cursor.execute(sqlRetriveKode, [noTransaksiForm])
        kodeUdahAda = namedtuplefetchall(cursor)

        for i in range(0, len(daftarItem), 3):
            cursor.execute(sqlDaftarItem, (noTransaksiForm, daftarItem[i], daftarItem[i+1], daftarItem[i+2]))
        
        sqlDelete = "DELETE from daftar_item where no_transaksi_sumber_daya=%s AND kode_item_sumber_daya =%s AND no_urut=%s;"
        for i in range(0, len(yangDiDelete), 2):
            cursor.execute(sqlDelete, [noTransaksiForm, yangDiDelete[i], yangDiDelete[i+1]])

        # sqlHargaSatuan = "SELECT harga_kumulatif FROM sidia.daftar_item WHERE no_transaksi_sumber_daya=%s"
        # cursor.execute(sqlHargaSatuan, [noTransaksiForm])
        # hargaSatuanTemp = namedtuplefetchall(cursor)

        # hargaTotal = 0
        # for i in range(0, len(hargaSatuanTemp)):
        #     hargaTotal += hargaSatuanTemp[i].harga_kumulatif
        # print(hargaSatuanTemp)
        # print(hargaTotal)

        # sqlPesananSumberDaya = "UPDATE sidia.pesanan_sumber_daya SET total_harga=%s;"
        # cursor.execute(sqlPesananSumberDaya, [hargaTotal])

        # sqlRiwayatStatusPesanan = "UPDATE sidia.RIWAYAT_STATUS_PESANAN SET tanggal=%s;"
        # cursor.execute(sqlRiwayatStatusPesanan, [str(tanggal)])

        sqlTransaksiSumberDaya = "UPDATE sidia.transaksi_sumber_daya SET tanggal=%s;"
        cursor.execute(sqlTransaksiSumberDaya, [str(tanggal)])

        return redirect('psd6:listPSD')

    cursor.execute("SELECT username_supplier from daftar_item di JOIN pesanan_sumber_daya psd ON no_transaksi_sumber_daya=nomor_pesanan JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode JOIN transaksi_sumber_daya tsd on no_transaksi_sumber_daya=nomor WHERE nomor_pesanan=%s", [nomorPesanan])
    supplier = namedtuplefetchall(cursor)

    cursor.execute("SELECT kode, psd.nomor_pesanan, username_admin_satgas, tsd.tanggal, username_supplier, di.no_urut, isd.nama, isd.harga_satuan, di.jumlah_item, psd.total_harga from daftar_item di JOIN pesanan_sumber_daya psd ON no_transaksi_sumber_daya=nomor_pesanan JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode JOIN transaksi_sumber_daya tsd on no_transaksi_sumber_daya=nomor WHERE nomor_pesanan=%s", [nomorPesanan])
    daftarItemList = namedtuplefetchall(cursor)
    cursor.execute("SELECT COUNT(*) from daftar_item WHERE no_transaksi_sumber_daya=%s", [nomorPesanan])
    noUrutMulaiDariSini = namedtuplefetchall(cursor)

    cursor.execute("SELECT kode FROM sidia.item_sumber_daya WHERE username_supplier=" + "'" + supplier[0].username_supplier + "';")
    kodeItemList = namedtuplefetchall(cursor)
    
    args = {"noTransaksi": nomorPesanan, "kodeItemList" : kodeItemList, "petugasCurrent": usernameAdmin, "supplier" : supplier[0].username_supplier, "daftarItemList" : daftarItemList, "noUrutMulaiDariSini" : noUrutMulaiDariSini[0].count + 1}
    return render(request, "updatePSD.html", args)

def deleteDaftarItem(request, nomorPesanan, noUrut):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidia")
    noUrutFinal = int(noUrut) + 1
    cursor.execute("DELETE from daftar_item where no_transaksi_sumber_daya =%s AND no_urut=%s;", [nomorPesanan, noUrut])
    cursor.execute("UPDATE daftar_item SET no_urut=%s where no_transaksi_sumber_daya =%s AND no_urut=%s;", [noUrut, nomorPesanan, noUrutFinal])
    return redirect('psd6:listPSD')

def detailPSD(request, nomorPesanan):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidia")
    # SEMUA
    # cursor.execute("SELECT psd.nomor_pesanan, username_admin_satgas, tsd.tanggal, username_supplier, di.no_urut, isd.nama, isd.harga_satuan, di.jumlah_item, psd.total_harga from daftar_item di JOIN pesanan_sumber_daya psd ON no_transaksi_sumber_daya=nomor_pesanan JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode JOIN transaksi_sumber_daya tsd on no_transaksi_sumber_daya=nomor WHERE nomor_pesanan=%s", [nomorPesanan])

    cursor.execute("SELECT nomor_pesanan, username_admin_satgas, tsd.tanggal, username_supplier from pesanan_sumber_daya JOIN transaksi_sumber_daya tsd on nomor_pesanan=nomor JOIN riwayat_status_pesanan ON nomor_pesanan=no_pesanan WHERE nomor_pesanan=%s GROUP BY nomor_pesanan, tsd.tanggal, username_supplier;", [nomorPesanan])
    detailPSD = namedtuplefetchall(cursor)

    cursor.execute("SELECT psd.nomor_pesanan, di.no_urut, isd.nama, isd.harga_satuan, di.jumlah_item, psd.total_harga from daftar_item di JOIN pesanan_sumber_daya psd ON no_transaksi_sumber_daya=nomor_pesanan JOIN item_sumber_daya isd ON kode_item_sumber_daya=kode WHERE nomor_pesanan=%s;", [nomorPesanan])
    detailDaftarItem = namedtuplefetchall(cursor)

    cursor.execute("SELECT total_harga from pesanan_sumber_daya WHERE nomor_pesanan=%s;", [nomorPesanan])
    totalHarga = namedtuplefetchall(cursor)
    totalHargaFinal = totalHarga[0].total_harga

    args = {"detailPSD" : detailPSD, "detailDaftarItem" : detailDaftarItem, "totalHarga" : totalHargaFinal}
    return render(request, "detail.html", args)