from django.urls import path
from .views import create_faskes, ajaxLokasiFaskes, ajaxTipeFaskes, read_faskes, delete_faskes, update_faskes
app_name = 'faskes_5'

urlpatterns = [
    path('createFaskes/', create_faskes, name='createFaskes'),
    path('ajaxLokasiFaskes/', ajaxLokasiFaskes, name='ajaxLokasiFaskes'),
    path('ajaxTipeFaskes/', ajaxTipeFaskes, name='ajaxTipeFaskes'),
    path('readFaskes/', read_faskes, name='readFaskes'),
    path('readFaskesS/', read_faskes, name='readFaskesS'),
    path('deleteFaskes/', delete_faskes, name='deleteFaskes'),
    path('updateFaskes/', update_faskes, name='updateFaskes'),

]
