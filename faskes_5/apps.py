from django.apps import AppConfig


class Faskes5Config(AppConfig):
    name = 'faskes_5'
