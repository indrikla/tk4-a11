from django import forms
import datetime
from collections import namedtuple
from django.db import connection

provinsiChoices = [
    ('Aceh','Aceh'),
    ('Sumatera Utara','Sumatera Utara'),
    ('Sumatera Barat','Sumatera Barat'),
    ('Riau','Riau'),
    ('Jambi','Jambi'),
    ('Sumatera Selatan','Sumatera Selatan'),
    ('Bengkulu','Bengkulu'),
    ('Lampung','Lampung'),
    ('Kepulauan Bangka Belitung','Kepulauan Bangka Belitung'),
    ('Kepulauan Riau','Kepulauan Riau'),
    ('DKI Jakarta','DKI Jakarta'),
    ('Jawa Barat','Jawa Barat'),
    ('Jawa Tengah','Jawa Tengah'),
    ('DI Yogyakarta','DI Yogyakarta'),
    ('Jawa Timur','Jawa Timur'),
    ('Banten','Banten'),
    ('Bali','Bali'),
    ('Nusa Tenggara Barat','Nusa Tenggara Barat'),
    ('Nusa Tenggara Timur','Nusa Tenggara Timur'),
    ('Kalimantan Barat','Kalimantan Barat'),
    ('Kalimantan Tengah','Kalimantan Tengah'),
    ('Kalimantan Selatan','Kalimantan Selatan'),
    ('Kalimantan Timur','Kalimantan Timur'),
    ('Kalimantan Utara','Kalimantan Utara'),
    ('Sulawesi Utara','Sulawesi Utara'),
    ('Sulawesi Tengah','Sulawesi Tengah'),
    ('Sulawesi Selatan','Sulawesi Selatan'),
    ('Sulawesi Tenggara','Sulawesi Tenggara'),
    ('Gorontalo','Gorontalo'),
    ('Sulawesi Barat','Sulawesi Barat'),
    ('Maluku','Maluku'),
    ('Maluku Utara','Maluku Utara'),
    ('Papua','Papua'),
    ('Papua Barat','Papua Barat')
]

class LoginForm(forms.Form):
    username = forms.CharField(label='Username:', max_length=50)
    password = forms.CharField(label='Password:', max_length=128)


class RegAdminSatgas(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama = forms.CharField(label='Nama', max_length=50)
    kelurahan = forms.CharField(label='Kelurahan', max_length=50)
    kecamatan = forms.CharField(label='Kecamatan', max_length=50)
    kabupatankota = forms.CharField(label='Kabupatan/Kota', max_length=50)
    provinsi = forms.ChoiceField(choices = provinsiChoices) 
    no_telp = forms.CharField(label='No Telepon', max_length=20)


class RegSupplier(forms.Form):
    organisasi = forms.CharField(label='Nama organisasi', max_length=50) 
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama = forms.CharField(label='Nama', max_length=50)
    kelurahan = forms.CharField(label='Kelurahan', max_length=50)
    kecamatan = forms.CharField(label='Kecamatan', max_length=50)
    kabupatankota = forms.CharField(label='Kabupatan/Kota', max_length=50)
    provinsi = forms.ChoiceField(choices = provinsiChoices) 
    no_telp = forms.CharField(label='No Telepon', max_length=20)

class RegPetugasDistribusi(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama = forms.CharField(label='Nama', max_length=50)
    kelurahan = forms.CharField(label='Kelurahan', max_length=50)
    kecamatan = forms.CharField(label='Kecamatan', max_length=50)
    kabupatankota = forms.CharField(label='Kabupatan/Kota', max_length=50)
    provinsi = forms.ChoiceField(choices = provinsiChoices) 
    no_telp = forms.CharField(label='No Telepon', max_length=20)
    no_sim = forms.CharField(label='No SIM', max_length=20)


class RegPetugasFaskes(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama = forms.CharField(label='Nama', max_length=50)
    kelurahan = forms.CharField(label='Kelurahan', max_length=50)
    kecamatan = forms.CharField(label='Kecamatan', max_length=50)
    kabupatankota = forms.CharField(label='Kabupatan/Kota', max_length=50)
    provinsi = forms.ChoiceField(choices = provinsiChoices) 
    no_telp = forms.CharField(label='No Telepon', max_length=20)