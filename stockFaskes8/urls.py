from django.urls import path, include
from . import views


urlpatterns = [
    path('create', views.createStokFaskes, name="createStokFaskes"),
    path('list', views.listStokFaskes, name="listStokFaskes"),
    path('delete/<kodeFaskes>/<kodeISD>/', views.deleteStokFaskes, name="deleteStokFaskes"),
    path('update/<kodeFaskes>/<kodeISD>/', views.updateStokFaskes, name="updateStokFaskes"),
    path('getDataItemSD', views.getDataItemSD, name="getDataItemSD"),
    path('getDataFaskes', views.getDataFaskes, name="getDataFaskes"),
#     path('logout', views.logout, name="logout"),
#     path('registerPilihan', views.registerPilihan, name="registerPilihan"),
#     path('regadminsatgas', views.register_adminsatgas, name="regadminsatgas"),
#     path('regsupplier', views.register_supplier, name="regsupplier"),
#     path('regpetugasdistribusi', views.register_petugasdistribusi, name="regpetugasdistribusi"),
#     path('regpetugasfaskes', views.register_petugasfaskes, name="regpetugasfaskes"),
#     path('dashboard', views.dashboard, name="dashboard")
]