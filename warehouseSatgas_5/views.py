from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from datetime import datetime
from django.contrib import messages



def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def ajaxidLokasiWarehouse(request):
    provinsi = request.GET.get('provinsi')
    print(provinsi)
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT ID FROM SIDIA.LOKASI WHERE PROVINSI = %s AND LOKASI.ID NOT IN (SELECT ID_LOKASI FROM SIDIA.WAREHOUSE_PROVINSI)", [provinsi])
        data = cursor.fetchall()
    list_idLokasi = []
    for i in data:
        list_idLokasi.append(i[0])

    return JsonResponse({'id': list_idLokasi})


def ajaxLokasiWarehouse(request):
    id_lokasi = request.GET.get('id_lokasi')
    print(id_lokasi)
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI FROM SIDIA.LOKASI WHERE ID = %s", [id_lokasi])
        data = cursor.fetchall()
    list_lokasi = []
    for i in data[0]:
        list_lokasi.append(i)

    return JsonResponse({'id': list_lokasi})

def create_warehousesatgas(request):
    form = CreateWarehouseSatgasForm()
    if request.method == "POST":
        provinsi = request.POST['provinsi']
        id_lokasi = request.POST['id_lokasi']
        lokasi = request.POST['lokasi']
        
        data_baru = [id_lokasi]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIDIA.WAREHOUSE_PROVINSI VALUES (%s)', data_baru)
            cursor.execute("SET SEARCH_PATH TO SIDIA;")
            sql = "SELECT DISTINCT JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI from SIDIA.LOKASI,SIDIA.WAREHOUSE_PROVINSI WHERE ID = ID_LOKASI;"
            cursor.execute(sql)
            result = dictfetchall(cursor)
            return render(request, 'readWarehouseSatgas.html', {'form': result})       
    return render(request, 'createWarehouseSatgas.html', {'form': form})

def read_warehousesatgas(request):
        role = request.session['role']
        if role == "admin_satgas":
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIDIA;")
                sql = "SELECT DISTINCT JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI from SIDIA.LOKASI,SIDIA.WAREHOUSE_PROVINSI WHERE ID = ID_LOKASI;"
                cursor.execute(sql)
                result = dictfetchall(cursor)
            return render(request, 'readWarehouseSatgas.html', {'form': result})

            
        elif role == "supplier":
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIDIA;")
                sql = "SELECT DISTINCT JALAN_NO,KELURAHAN,KECAMATAN,KABKOT,PROVINSI from SIDIA.LOKASI,SIDIA.WAREHOUSE_PROVINSI WHERE ID = ID_LOKASI;"
                cursor.execute(sql)
                result = dictfetchall(cursor)
            return render(request, 'readWarehouseSatgasS.html', {'form': result})
        else:
            pass

def delete_warehousesatgas(request):
    if request.method == 'GET':
        if request.GET.get('provinsi') is not None and request.GET.get('kabkot') is not None and request.GET.get('kecamatan') is not None and request.GET.get('kelurahan') is not None and request.GET.get('jalan_no') is not None :
            provinsi = request.GET.get('provinsi')
            kabkot = request.GET.get('kabkot')
            kecamatan = request.GET.get('kecamatan')
            kelurahan = request.GET.get('kelurahan')
            jalan_no = request.GET.get('jalan_no')

            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIDIA.WAREHOUSE_PROVINSI WHERE ID_LOKASI IN (SELECT ID FROM SIDIA.LOKASI JOIN SIDIA.WAREHOUSE_PROVINSI ON ID = ID_LOKASI WHERE PROVINSI = %s AND KABKOT = %s AND KECAMATAN = %s AND KELURAHAN = %s AND JALAN_NO = %s)", [provinsi,kabkot,kecamatan,kelurahan,jalan_no])
                messages.success(request, ('Data berhasil dihapus'))

    return HttpResponseRedirect(reverse('warehouseSatgas_5:readWarehouseSatgas'))

def update_warehousesatgas(request):

    if request.method == 'GET':
        print(request.GET.get('provinsi'))

        if request.GET.get('provinsi') is not None:
            provinsi = request.GET.get('provinsi')

            with connection.cursor() as cursor:
                cursor.execute("SELECT ID,JALAN_NO,PROVINSI,KABKOT,KECAMATAN,KELURAHAN FROM SIDIA.LOKASI WHERE ID IN (SELECT ID_LOKASI FROM SIDIA.WAREHOUSE_PROVINSI) AND PROVINSI = %s", [provinsi])
                result = dictfetchall(cursor)

            data_warehouse = {}
            data_warehouse['id_lokasi'] = result[0]['id']
            data_warehouse['provinsi'] = result[0]['provinsi']
            data_warehouse['lokasi'] = result[0]['jalan_no'],result[0]['kecamatan'], result[0]['kelurahan'],result[0]['kabkot']
            form = UpdateWarehouseSatgasForm(initial=data_warehouse)
    
    else:
        id_lokasi = request.POST['id_lokasi']
        provinsi = request.POST['provinsi']
        lokasi = request.POST['lokasi']

        data_update = [id_lokasi,provinsi]
        with connection.cursor() as cursor:
            cursor.execute("UPDATE SIDIA.WAREHOUSE_PROVINSI SET id_lokasi = %s WHERE id_lokasi IN (select id FROM SIDIA.LOKASI WHERE provinsi=%s)", data_update)        
        return redirect('warehouseSatgas_5:readWarehouseSatgas')
    return render(request, 'updateWarehouseSatgas.html', {'form': form})