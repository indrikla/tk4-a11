from django.urls import path
from .views import ajaxidLokasiWarehouse, create_warehousesatgas, ajaxLokasiWarehouse,read_warehousesatgas, delete_warehousesatgas, update_warehousesatgas
app_name = 'warehouseSatgas_5'

urlpatterns = [
    path('createWarehouseSatgas/', create_warehousesatgas, name='createWarehouseSatgas'),
    path('ajaxidLokasiWarehouse/', ajaxidLokasiWarehouse, name='ajaxidLokasiWarehouse'),
    path('ajaxLokasiWarehouse/', ajaxLokasiWarehouse, name='ajaxLokasiWarehouse'),
    path('readWarehouseSatgas/', read_warehousesatgas, name='readWarehouseSatgas'),
    path('readWarehouseSatgasS/', read_warehousesatgas, name='readWarehouseSatgasS'),
    path('deleteWarehouseSatgas/', delete_warehousesatgas, name='deleteWarehouseSatgas'),
    path('updateWarehouseSatgas/', update_warehousesatgas, name='updateWarehouseSatgas'),
]
